# Boiler plate portal #

Files of interest :

    application/config/development/settings.json.sample
    application/config/staging/settings.json
    application/config/production/settings.json

    application/controllers/Email.php
    application/controllers/Portal.php

    application/libraries/Sparkapi.php
    application/libraries/WFS_Form_Validation.php

    application/models/Subscriber.php

    application/views/*

`settings.json` files must follow this schema:


    {
        "base_url" : "https://host.name/",
        "log_threshold" : 0|1|2,
        "api_settings" :
        {
            "spark_api" :
            {
                "endpoint" : "https://api.address:port/"
            },
            "oauth" :
            {
                "client_id" : "<string>",
                "client_secret" : "<string>",
                "username" : "<string>",
                "password" : "<string>",
                "scope" : "<string>"
            }
        },
        "form" : [],
        "marketing_options" : [],
        "agree_terms" : true|false,
        "default_hotspot_ip" : "<IP address>",
        "hotspot_details" : {
            "<IP address of hotspot>" : {
                "interface_id" : ""|<id>
            }
        },
        "activation_range" : {
            "location_id" : <id>,
            "location_type" : "customer|hotspot|zone|subzone|interface"
        },
        "login_prefix" : "<string>",
        "validation_access_time" : <in seconds>,
        "validated_access_time" : <in seconds>,
        "bandwidth" : {
            "up" : <bps>,
            "down" : <bps>
        },
        "landing_page" : "<url>",
        "welcome_email" : {
            "from" : "<email address>",
            "subject" : "<string>"
        },
        "friendly_wifi" : true|false
    }

Any `<placeholder>` should be replaced with the desired value.

The `default_hotspot_ip` value should be the same as the hotspot ip in SPARK.

the `form` array can contain 1 or more objects that follow this schema:

where the field type is **not** a `select` box:

    {
        "name" : "<Spark API field>",               // this must match exactly the name of a field expected by the Spark API /subscriber resource
        "type" : "text|tel|email",                  // HTML <input> type
        "label" : "<string>",                       // how the field is labelled
        "placeholder" : "<string>",                 // placehold text that gives an indication of the expected input
        "required" : true|false,                    // is this field mandatory?
        "client_validation_regex" : "<string>",     // a regular expression for client side validation, or blank if no validation required, remember that \ is an escape character in JSON
        "client_validation_error" : "<string>",     // error message shown to user if field doesn't pass validation, can be omitted if the regex string is empty
        "server_validation_rules" : "<string>",     // the server side validation rule to use (see CodeIgniter documentation and application/libraries/WFS_Form_Validation.php)
        "server_validation_label" : "<string>",     // sent back with any form validation errors to the user
        "on_change" : "<javascript>"                // javascript to execute when field data changes, would expect to declare a function in view code and call that here eg. someFunction(); would be the value given here
    }

in the case of a `select` box field:

    {
        "name" : "<Spark API field>",               // this must match exactly the name of a field expected by the Spark API /subscriber resource
        "type" : "select",
        "label" : "<string>",                       // how the field is labelled
        "server_validation_rules" : "",             // leave blank
        "client_validation_error" : "",             // error message if field doesn’t pass validation
        "on_change" : "<javascript>",               // javascript to execute when field selection changes, would expect to declare a function in view code and call that here eg. someFunction(); would be the value given here
        "default" : "<value>",                      // the default selected option (that matches one of the options below)
        "required" : true|false,                    // is this field mandatory
        "options" : [                               // as many options as necessary
            {
                "value" : "<string>",               // the value posted
                "label" : "<string>",               // option presented to user
                "disabled" : true                   // optional - unselectable i.e. separator etc.
            },
            {
                "value" : "<string>",
                "label" : "<string>",
                "disabled" : true
            }
        ]
    }

Given that this is an email validation user journey, one of the form fields *MUST* be :

    {
        "name" : "email",
        "type" : "email",
        "label" : "<string>",
        "placeholder" : "<string>",
        "required" : true,
        "client_validation_regex" : "^[a-zA-Z0-9\\._%\\+\\-]+@[a-zA-Z0-9\\.\\-]+\\.[a-zA-Z]{2,}$",
        "client_validation_error" : "<string>",
        "server_validation_rules" : "valid_email",
        "server_validation_label" : "email address",
        "on_change" : ""
    }

It is also expected that the `first_name` field is collected, if not you will have to remove `Not <?php echo $user; ?>?` from the relevant views.

The `marketing_options` array can contain between 0 and 3 objects that follow this schema:

    {
        "name" : "allow_email_marketing|allow_mobile_marketing|allow_postal_marketing",
                                        // which marketing option to be recorded
        "label" : "<string>",           // how the check box is labelled
        "client_validation_error" : "<string>", // how the error message is labelled
        "required" : false|true,        // required state
        "default" : "on|off",           // default state
        "on_change" : "<javascript>"    // javascript to execute when field data changes, would expect to declare a function in view code and call that here eg. someFunction(); would be the value given here
    }

### Setup notes ###

`composer install` to download the dependencies.

Hotspot(s) must be set up with `Allow subscriber login`, it is better to disable `Allow free use subscriber`.  
`Display Settings` should all be set to `No` to avoid any problems.  
`Display agree to Ts and Cs` (under `Portal` tab) must be set to `No`.  

An `Administrative role` should be set up with the following permissions :

    activateSubscriber
    createSession
    createSubscriber
    getSubscriber
    getSubscribers
    sendAnalyticsData
    updateSubscriber

Additionally all `options` and `optionsBase` should be granted to avoid problems.

The name of this role is the `Scope` used in the oauth section of the settings.

Create an `Administrative user` and set the customer to the same used for the hotspot(s), this is the `username` and `password` that you provide in the oauth section.

Create an `Oauth client` and set the `user` to the `Administrative user` and the `Role` to the `Administrative role` that you created previously, these credentials are the `client_id` and `client_secret` that you provide in the oauth section.

### Troubleshooting ###

If necessary adjust the `log_threshold` in settings.json

    0 = No logging
    1 = Error logging
    2 = Debug logging

For sites in production the logs will be found on the AWS "services" machine, in `/var/log/redirect`  
For sites in development you will need to configure `/etc/rsyslog.conf` with

    local5.*        /var/log/boilerplate.log

after restarting `rsyslog`, logs will appear at `/var/log/boilerplate.log`
