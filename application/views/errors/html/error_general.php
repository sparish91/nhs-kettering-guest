<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $status_code; ?></title>
<style type="text/css">
body {
    background-color: #444;
    color: #fff;
    font-family: sans-serif;
    margin: 0;
    padding: 0;
    font-size: 16px;
}
a {
    color: #aaa;
    text-decoration: none;
}
a:hover {
    text-decoration: underline;
}
.wrapper {
    position: relative;
    height: 100vh;
    width: 100vw;
}
.centered {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
}
.support {
    color: #777;
    font-size: 13px;
}
</style>
</head>
<body>
    <div class="wrapper">
        <div class="centered">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAAtCAYAAAAHpEG5AAAACXBIWXMAAAS6AAAEugG8WOxKAAAIZklEQVR42u1cu24bRxQ9oiQjKQLuH5DOB0RrII0KgssmSJOI+QKuKwIMAdFtGlFfIAogCLDy6gtCfYFXUGEgRUIVSWeITBE4RRKunSZ+iCk0a41Gc+exXNqkMQdYwOLuDu/OnDlz7t0xN+bzORwc1gn3nzz1AEQAAgDly9ruLD1XcN3jsIaIAOwBKALw+ROO0A7rps4dRuYUY/78hrMcDmtEZh/AL9xH08vabtkptMO6+uaR8PFYvM4R2mGdfHPJEdrhY1DnUPDNjtAOa+2be8TpO4R2SaHDqvvmGMCO5HRyWdv1nEI7rBN6BJml6uwI7bDqvrmhuCR2hHb4GHyzU2iHtfPNEa5faztCO3zUvplPCCeO0A6rrs51jW9WqjMAbPUHwwDX2/BuzZJ2qzkTL+4Phh6ADvfRqN1qShvvD4YdAJ7s2v5g2MPtXVJRu9WMuHvrwnkT8O13hWe61b6AOjt8iTJcsM4b4e5rVx7isyrjVA2IAA9AyLUdU8kQ8VymfThm7c4UcXQMn6HMYhbbH2nIXGZWwwRkH2xB/koRALoCQX3WEO9tDvqD4UORLGySHMk6uD8YlgHsC+f89GEYmX/MMME7LMYZgAOqfclnquVthx0NAGdsoCaSJXLfIs4DAFNGENUgl1l/l4R7HxkkTFn6MGHtdgkC7QgT2Jf0hSeJOW3bM5joRcNYJ9QJynIEks9C4gtDw/tn3ECJKApEy4Iia9vXtJ9+R2zg1XhUmdJ4krZsUWKECzWk1AoNAT9j/x0QKrlD9LUIShwjjTr3LMdirFLoWOJbyoYkTQfapENj3ezKCbr2PUs1EAdypOgLWzxm8caEjaFiqOuW8AXQYITpWd4XQr7f4kLxLKlvvrXCXc1fYf7mN+n127h6/ezTH+o4R10mmgWiM0vML/PemZxBzGLoJkQMAO1WcwLgWDh3mMNAnLRbzdigfUr50s4/Y0eimMDlHAnUJQShpCHPMtGxvJ6qGyesv41985v5v/jsv1MU3/wsPb6Z/7TNVhLZcbSlMNg+d06nSIHQzh3yt1vNmPt3x7LTTnTLlkX7daLjA8lS1pX48anBKiCLt8wGvSiZIJ6QkOn6Zk9yT5Y+9Ngz7kgskW+RvEYKOzqx8c1v3z7HxvwlndRs/qUMZKvdak76g+FUogi2hKbUGkzxxASTn9EdqlqS2giesDpo2pclJz1i8Lrs+n3uOUwm4kQhFI814gGVqglksbEFVExjAJdEUmpCaMr/HqtsEeWbP7n6W/lln2++VJ1+VFCUQXyLDq6a2A3h7yp32Po1k7KOTfuhIgvvALgP4AGh4jaIDIlalFihZdmOyQKJZZ2o8Fj75hT35mpClwrk+VNUkp6K0GWmdmWNnxOV2TcgdBHLRdFyAEvs84gNRCCsSpMFiaya7CaWKJR4+p0FKkImMeme1ycmqM43e6qJvX31hzrYwu+yj6fpBN9SELqqsBuJhDSpj/ZV/jYjQsLKgPnIUPYiSKHeDWISNCTnLtg9vRwqNF0Dcu1JBit9MdGQEL1jQdyA8NBZqkVHihVmksFv47XGbny79YIWgUoye0doykczLxoQA3NE+Ghf5Z8zoqRZJSJD3wlOhU3rnunLlX3mC00IFAhE8RTVlYQTlDqROCEHQjdg9lo5jSnLiqT0zVxCK8WVhtBfbT+X+mZUknexbmmUS0bopN1q9vqDoUjoKrMnRY3dWAY8y+vT1WTH8r59pnQmOUXVsM2RproRcdeJK+OyatJZcxqTOGpUkeH7wrPv7t/784vtjVd3rcrmC3y5+evdYkMluRWrjtCBRFViTnmrBonK+yC0LWZssnZYzDbE3mP3RDnEkXAkltWep4JSUiqdJ6FPYPY2klr90u0HUlzWdmOSE+dfB5Z9d0dYCpokoKEgqCyogyX4ZxtSZFEin1Uxarh+AXPIJuvUMnGzxZQJxkyhziODKslehhWKwqFF9WRKWMNFJrpNkvvON0sVut1qjvuDYWJQgeAJfaC59iyvjm63mt0lTooJ5HXaAPI6a3mB77pggx4JSlYnLI7JxqfQwCacsedT+XmbFzUhU/JqLivYebFsUf06RCWRCmXB0h5MuRcUJknDKtqNlDwRI3Goib9LJIq6ZbsmHA8AbODmNfFMIMcipczQcCy6uNkpNyWSfc+yHxPFyrcMdT5DJSHFzZbQMafoM8gL/qtO6BDXO90aTKUeayxLQKisTvFj4Rgv0cLY1qRnxEQtWtq3GRF7kQmGlzOhE11fZSa0yfXvwT/npWZHuPm9YQ83NduIWPLHOcZThqKUlbNKi35cptIdSyLGuLsZLJ1kNtUSk4RQ6ptJQjM7keRE6DOsJqiYGwCeAPgH13sbnoCu20Y5xkNtlqopjtMcCI2cVDqdBBdEn5rGpVNo0jerFFo14FO2NdOU0Kvqn3sGlkHnj/N8Nqq6ESuOiCCirXXJS6UX89P6hFDpm7MSOpZYCpWPXlVCz5hqTDOSuZNjLNS+Z91SPSLIE2ac4LLJYVtVmhB9UzRY0XyNbzZ+rgIxa08l9qGrUBiRHIca/3yiWMLFwUpg/+LgRGMRxqwTjzUWi08CH7KOnUmePxGu7VmQYCr5LhOPHilEZGQRU0SIEh/DQ8NVKpL0PaAvB/rK3KCSTEwHnvyxRraPwzNJ7tj/aEmDmmn2Nqf3BNT17BV6OR10idXBIu0TCUmgqFbovp+Pdwy7eu6tZ4XdBihPIEO8QEyqtlLSeRarbyD040RhOWLItwoco5JYrYju10cdPjzOixOJ9bpAJbHeHut+aMbhQ5PZg/xnDzLV5h2hHT40/EV9syO0wyohkPjmzLsHHaEdVkmhL2yTQEdoh1UldGbf7AjtsEqIke5pz+ibefwP5uzfVbk/5mAAAAAASUVORK5CYII=" alt="404" />
            <h1>Oh no!</h1>
            <h2><?php echo $heading; ?></h2>
            <p><?php echo $message; ?></p>
            <p class="support">WiFi support : <a href="tel:03448489555">0344 848 9555</a></p>
        </div>
    </div>
</body>
</html>
