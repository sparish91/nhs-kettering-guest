<div class="row">
    <div class="col-md-7">
        <h1 class="header">Incorrect email supplied</h1>
        <!--<div class="footer">
            <p>Wifi Helpdesk 0344 848 9555</p>
            <img src="../../assets/images/wifilogo.png">
        </div>-->
    </div>
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-5 col-md-offset-0 right-content">
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="p-head">
                    <p>
                        The email you submitted does not match the email you used to register.
                    </p>
                    <a href="/reset" class="panel-links">Please register your device again</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-md-7 footer-info-main">
    <a href="http://www.wifispark.com/"><img class="footer-image" src="/assets/images/wifilogo.png"></a>
    <p>WiFi Helpdesk: 0344 848 9555</p>
    <?php
    if ($this->config->item('settings_parsed')->friendly_wifi) :
    ?>
    <a href="http://www.friendlywifi.com/"><img class="center-block friendly" src="/assets/images/friendly_wifi.png" alt="Friendly WiFi" /></a>
    <?php
    endif;
    ?>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
