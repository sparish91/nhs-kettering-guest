<html>
<head>
<title>Redirecting</title>
<style>
#link, #done {
    display: none;
}
</style>
</head>
<body>
  <div class="row">
    <div class="col-md-7">
        <h1 class="header">You are connected</h1>
        <h4 id="done">Please close this window to continue</h4>
        <!--<div class="footer">
            <p>Wifi Helpdesk 0344 848 9555</p>
            <img src="../../assets/images/wifilogo.png">
        </div>-->
    </div>
  </div>
<a href="<?php echo $_SESSION['url']; ?>" id="link">Link</a>
<script src="/assets/lib/js/jquery.min.js"></script>
<script type="text/javascript">
<!--
$('document').ready(function () {
    setTimeout(function () {
        document.getElementById("link").click();
    }, 2000);
    setTimeout(function () {
      $("#done").show();
    }, 2500);
});
-->
</script>
</body>
</html>
