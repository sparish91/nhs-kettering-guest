<div class="row">
    <div class="col-md-7">
        <h1 class="header">There appears to be an error</h1>
        <!--<div class="footer">
            <p>Wifi Helpdesk 0344 848 9555</p>
            <img src="../../assets/images/wifilogo.png">
        </div>-->
    </div>

    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-5 col-md-offset-0 right-content">
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="p-head">
                    <p>We're sorry but there appears to have been a problem, please try again later.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-md-7 footer-info-main">
    <a href="http://www.wifispark.com/"><img class="footer-image" src="/assets/images/wifilogo.png"></a>
    <p>WiFi Helpdesk: 0344 848 9555</p>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
