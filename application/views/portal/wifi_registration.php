<div class="registration center-block">
  <div class="nhs-header col-xs-12 col-md-6">
    <h1 id="nhs-header"> NHS Staff WiFi Access</h1>
  </div>
<?php include("include/formerrors.php"); /////////////////////////// form error messages ?>
<?php
    // show flashdata messages if necessary
    if($this->session->flashdata('msg')):
?>
<div class="row">
  <div class="nhs-header col-xs-12">
    <h1 id="nhs-header"> NHS Staff WiFi Access</h1>
  </div>
    <div class="col-xs-12 col-sm-6">
        <div class="alert alert-warning" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
</div>
<?php
    endif;
?>
<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5 right-content">
            <div id="breadcrumbs-nav">
                <div class="wizard">
                    <div class="wizard-inner">
                        <div class="connecting-line"></div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a><span class="square-step">1</span></a>
                                <label class="breadcrumb-label">Register</label>
                            </li>
                            <li role="presentation">
                                <a><span class="square-step">2</span></a>
                                <label class="breadcrumb-label">Validate Email</label>
                            </li>
                            <li role="presentation">
                                <a><span class="square-step">3</span></a>
                                <label class="breadcrumb-label">Validated</label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


<?php include("include/formgenerator.php"); /////////////////////////// form itself ?>
</div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
<script src="/assets/lib/js/jquery.FlowupLabels.js"></script>
<script src="/assets/lib/js/validator.min.js"></script>
<script type="text/javascript">
    $(window).load(function () {
        $('form').validator().off('input.bs.validator change.bs.validator');
        $('form').validator().on('submit', showSpinner);
        $('.FlowupLabels').FlowupLabels();
        // since javascript is executing we can remove the no-js class from the body
        $('body').removeClass('no-js');
        // this makes sure that the labels get out of the way when form is filled by autocomplete
        $('input').on('change', function() {
            $('.fl_input').trigger('blur.flowupLabelsEvt');
        });
        $('#zip_code').on('blur', function() {
            // upper case and format the zip_code field then trigger validation
            var postcode = $('#zip_code').val().toUpperCase();
            if (postcode.indexOf(' ') == -1) {
                var numDigits = postcode.replace(/\D/g, '').length;
                if (numDigits == 2) {
                    // postcode in format EX2 8PW
                    postcode = postcode.substring(0,3) + ' ' + postcode.substring(3);
                } else {
                    // postcode in format RM13 6BH
                    postcode = postcode.substring(0,4) + ' ' + postcode.substring(4);
                }
            }
            $('#zip_code').val(postcode);
            $('#zip_code').trigger('change');
        });
        zipCodeReg = $('#zip_code').attr('pattern');
    });
    $('input[type="email"]').focusout(function(){
      $('form').validator();
    });
    function showSpinner(e) {
        // don't let the form be submitted if there is no postcode when there should be
        if (!e.isDefaultPrevented() && ('GB' == $('#country').val()) && ('' == $('#zip_code').val())) {
            $('#zip_code-wrapper').addClass('has-error');
            $('#zip_code').focus();
            e.preventDefault();
        }
        // if the form is valid then cover the display and show a spinner
        if (!e.isDefaultPrevented()) {
            $('body').append('<div class="spinner-container"><div class="spinner"><img src="/assets/images/spinner.gif" /></div></div>');
            $('body').addClass('noscroll');
            $('#registering').attr('disabled', 'disabled');
        }
    }
    $('#zip_code').prop('required', true);
    function changeCountry() {
        if ('United Kingdom' == $('#country').val()) {
            // put back the postcode validation rule and show the field again
            $('#zip_code').attr('pattern', zipCodeReg);
            $('#zip_code').prop('required', true);
            $('form').validator().off('submit');
            $('form').validator('destroy');
            $('form').validator().on('submit', showSpinner);
            $('form').validator('validate');
            $('#zip_code-wrapper').show();
        } else {
            // the opposite
            $('#zip_code').removeAttr('pattern')
            $('#zip_code').removeAttr('required')
            $('#zip_code').val('');
            $('form').validator().off('submit');
            $('form').validator('destroy');
            $('form').validator().on('submit', showSpinner);
            $('form').validator('validate');
            $('#zip_code-wrapper').hide();
        }
    }
</script>
<div class="col-xs-12 col-md-7 footer-info-main">
    <a href="http://www.wifispark.com/"><img class="footer-image" src="/assets/images/wifilogo.png"></a>
    <p>WiFi Helpdesk: 0344 848 9555</p>
    <?php
    if ($this->config->item('settings_parsed')->friendly_wifi) :
    ?>
    <a href="http://www.friendlywifi.com/"><img class="center-block friendly" src="/assets/images/friendly_wifi.png" alt="Friendly WiFi" /></a>
    <?php
    endif;
    ?>
</div>
