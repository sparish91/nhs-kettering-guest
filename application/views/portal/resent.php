<div class="row">
    <div class="col-md-7">
        <h1 class="header">Validation email has been resent.</h1>
        <!--<div class="footer">
            <p>Wifi Helpdesk 0344 848 9555</p>
            <img src="../../assets/images/wifilogo.png">
        </div>-->
    </div>
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-5 col-md-offset-0 right-content">
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="p-head">
                    <p>
                        Please check your email and click on the validation link to be granted <?php echo $validated_days; ?> days of free internet access.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-md-7 footer-info-main">
    <a href="http://www.wifispark.com/"><img class="footer-image" src="/assets/images/wifilogo.png"></a>
    <p>WiFi Helpdesk: 0344 848 9555</p>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
