<div class="row">
  <div class="nhs-header col-xs-12 col-md-6">
    <h1 id="nhs-header"> NHS Staff WiFi Access</h1>
  </div>
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-5 col-md-offset-0 right-content">
          <div id="breadcrumbs-nav">
              <div class="wizard">
                  <div class="wizard-inner">
                      <div class="connecting-line"></div>
                      <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active">
                              <a><span class="square-step">1</span></a>
                              <label class="breadcrumb-label">Register</label>
                          </li>
                          <li role="presentation" class="active">
                              <a><span class="square-step">2</span></a>
                              <label class="breadcrumb-label">Validate Email</label>
                          </li>
                          <li role="presentation">
                              <a><span class="square-step">3</span></a>
                              <label class="breadcrumb-label">Validated</label>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>


                    <div class="panel panel-default">
                        <div class="panel-header panel-h1">
                            <div class="p-head">
                                <h1>Email not validated</h1>
                            </div>
                        </div>
                        <div class="panel-header">
                          <div class="p-head">
                        <p>This device has previously been registered and you should have received an email with a validation link.  Please check your email (including the Spam folder)</p>
                      </div>
                    </div>
                        <div class="panel-body">
                            <div class="validate-emails">
                                <a href="/resend" id="resend" class="panel-links">Resend activation email</a>
                                <a href="/reset" class="panel-links">Register device again</a>
                            </div>
                        </div>
                    </div>
</div>
</div>

<div class="col-xs-12 col-md-7 footer-info-main">
    <a href="http://www.wifispark.com/"><img class="footer-image" src="/assets/images/wifilogo.png"></a>
    <p>WiFi Helpdesk: 0344 848 9555</p>
    <?php
    if ($this->config->item('settings_parsed')->friendly_wifi) :
    ?>
    <a href="http://www.friendlywifi.com/"><img class="center-block friendly" src="/assets/images/friendly_wifi.png" alt="Friendly WiFi" /></a>
    <?php
    endif;
    ?>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
<script src="/assets/lib/js/jquery.FlowupLabels.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#resend').attr('href', '#').on('click', function() {
            if ($('#verify').length == 0) {
                $('#resend').after('<div id="verify"><form action="/verify" method="post"><div class="form-group fl_wrap"><input type="email" name="email" class="form-control fl_input" placeholder="Email Address" required pattern="^[a-zA-Z0-9\._%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$" /></div><button class="verify-btn" type="submit" style="margin:20px 0;">Verify Email</button></form></div>');
                $('#verify').hide().fadeIn(500);
                $('.FlowupLabels').FlowupLabels();
                $('form').on('submit', showSpinner);
            }
        });
        $('body').removeClass('no-js');
    });
    function showSpinner() {
        $('body').append('<div class="spinner-container"><div class="spinner"><img src="/assets/images/spinner.gif" /></div></div>');
        $('body').addClass('noscroll');
    }
</script>
