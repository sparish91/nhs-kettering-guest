<div class="content center-block">
<div class="row heading">
    <div class="col-xs-12">
        WiFi Support
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <p>
            Welcome to the staff WiFi service brought to you by WiFi SPARK. The service is free and very easy to use. Simply follow the instructions below to connect:
        </p>
        <ol>
            <li>
                Ensure you have read and agreed to the <a class="terms-modal" href="/terms_of_use">terms of use</a>.
            </li>
            <li>
                Please enter your staff email address in the form provided. Please ensure these are correct before proceeding.
            </li>
            <li>
                Click the <strong>Register</strong> button. You will be granted <?php echo $validation_minutes; ?> minutes of internet access and sent an email with an activation link.
            </li>
            <li>
                Check your email to find the activation link we have sent you. Click the activation link within <?php echo $validation_minutes; ?> minutes of registering to validate your account. Your device will then be provisioned with <?php echo $validated_days; ?> days of time. After this time you can simply re-register your device
            </li>
        </ol>
        <p>
            We hope you enjoy using this service.
        </p>
        <p>
            If you require any WiFi assistance, please contact WiFi SPARK on <a href="tel:03448489555">0344 848 9555</a>.
        </p>
        <p>
            The security of your connected device is your responsibility, and we recommend that your firewall and antivirus software is correctly configured and up to date.
        </p>
    </div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
