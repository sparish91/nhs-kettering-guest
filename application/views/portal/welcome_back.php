<div class="row">
    <div class="col-md-7">
        <h1 class="header">Welcome back</h1>
        <!--<div class="footer">
            <p>Wifi Helpdesk 0344 848 9555</p>
            <img src="../../assets/images/wifilogo.png">
        </div>-->
    </div>
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-5 col-md-offset-0 right-content">
                    <div class="panel panel-default" style="margin-top:20px;">
                        <div class="panel-header">
                            <div class="p-head">
                                <h1>Welcome back <?php echo $user; ?></h1>
                                <p>
                                    To continue to the Internet, simply click the button below.
                                </p>
                                <p><a href="/reset">Forget this device</a></p>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <a class="btn connect" href="/proceed">Continue</a>
                                </div>
                            </div>
                        </div>
                    </div>
</div>
</div>

<div class="col-xs-12 col-md-7 footer-info-main">
    <a href="/go/http%3A%2F%2Fwww.wifispark.com%2F"><img class="footer-image" src="/assets/images/wifilogo.png"></a>
    <p>WiFi Helpdesk: 0344 848 9555</p>
    <?php
    if ($this->config->item('settings_parsed')->friendly_wifi) :
    ?>
    <a href="http://www.friendlywifi.com/"><img class="center-block friendly" src="/assets/images/friendly_wifi.png" alt="Friendly WiFi" /></a>
    <?php
    endif;
    ?>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('body').append('<img src="http://1.1.1.1/" style="display: none;" />');
});
</script>
