<div class="row">
    <div class="col-md-7">
        <h1 class="header">Thank you for registering.</h1>
        <!--<div class="footer">
            <p>Wifi Helpdesk 0344 848 9555</p>
            <img src="../../assets/images/wifilogo.png">
        </div>-->
    </div>
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-5 col-md-offset-0 right-content">
            <div id="breadcrumbs-nav">
                <div class="wizard">
                    <div class="wizard-inner">
                        <div class="connecting-line"></div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a><span class="square-step">1</span></a>
                                
                            </li>
                            <li role="presentation" class="active">
                                <a><span class="square-step">2</span></a>
                                
                            </li>
                            <li role="presentation">
                                <a><span class="square-step">3</span></a>
                                
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

                    <div class="panel panel-default">
                        <div class="panel-header">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>Thank you for registering.</p>
                                    <p>
                                        You now have <?php echo $validated_days; ?> days of free internet access.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <a class="btn connect" href="/portal/iosredirect">Proceed to internet</a>
                                </div>
                            </div>

                        </div>
                    </div>
</div>
</div>

<div class="col-xs-12 col-md-7 footer-info-main">
    <a href="http://www.wifispark.com/"><img class="footer-image" src="/assets/images/wifilogo.png"></a>
    <p>WiFi Helpdesk: 0344 848 9555</p>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
