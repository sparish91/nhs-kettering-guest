<?php
    echo form_open('portal/register');
?>
        <div class="panel panel-default">
            <div class="panel-header text-center" id="grey-bg">
                <h1>Register</h1>
                <p>This service is available to NHS staff. To register for WiFi access, please enter your details below.</p>
            </div>
            <div class="panel-body">
            <div class="standard-inputs">
<?php
    // get the form details from parsed json settings
    $form_items = $this->config->item('settings_parsed')->form;
    $showRequiredMsg = false;
    foreach ($form_items as $form_item) {
        switch ($form_item->name) {
            case 'first_name':
                $columnWidth = 'col-xs-6';
                $style = 'padding-right:0;';
                $mobile_padding = 'mobile-padding-left';
                break;
            case 'last_name':
                $columnWidth = 'col-xs-6';
                $style = '';
                $mobile_padding = 'mobile-padding-right';
                break;
            case 'email':
                $columnWidth = 'col-xs-12';
                $style = '';
                $mobile_padding = 'mobile-padding-left mobile-padding-right';
                break;
        }
?>
            <div class="form-group has-feedback<?php
            if (form_error($form_item->name)) echo ' has-error';
            echo ('select' == $form_item->type)?' sel_wrap':' fl_wrap';
            echo ' '.$columnWidth; ?>" id="<?php echo $form_item->name; ?>-wrapper" style="z-index:10000;>">
                <?php
                    if ('select' != $form_item->type) :
                ?>
                <input class="form-control fl_input"
                 type="<?php echo $form_item->type ?>"
                 id="<?php echo $form_item->name; ?>"
                 name="<?php echo $form_item->name; ?>"
                 value="<?php echo set_value($form_item->name); ?>"
                 placeholder="<?php echo $form_item->placeholder; ?>"
                 onchange="<?php echo $form_item->on_change; ?>"
                 data-error="<?php echo $form_item->client_validation_error; ?>"
                 <?php
                    if ($form_item->client_validation_regex) {
                        echo 'pattern="' . $form_item->client_validation_regex . '"';
                    }
                    if ($form_item->required) {
                        echo ' required';
                        $showRequiredMsg = true;
                    }
                 ?> />
                <?php
                    else :
                        echo '<select id="' . $form_item->name . '"';
                        echo ' name="' . $form_item->name . '"';
                        echo ' onchange="' . $form_item->on_change . '"';
                        if ($form_item->required) {
                            echo ' required';
                            $showRequiredMsg = true;
                            echo ' data-error="' . $form_item->client_validation_error . '"';
                        }
                        echo ' class="form-control">';
                        foreach ($form_item->options as $option) {
                            echo '<option value="' . $option->value . '"' . (($option->value == $form_item->default)?'selected':'') . '>' . $option->label . '</option>';
                        }
                        echo '</select>';
                ?>
                <?php
                    endif;
                ?>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
<?php
    }   // foreach
?>
            </div>
            <div class="top-gap col-xs-12">
<?php
    // get marketing options from parsed json settings
    $marketing_options = $this->config->item('settings_parsed')->marketing_options;
    foreach ($marketing_options as $option) {
         $isRequired = ($option->required)?'required data-error="' . $option->client_validation_error . '"':'';
?>
         <div class="form-group has-feedback">
            <input type="checkbox" <?php echo $isRequired; ?> id="<?php echo $option->name; ?>" name="<?php echo $option->name; ?>" onclick="<?php echo $option->on_change; ?>" checked />
            <label for="<?php echo $option->name; ?>" class="marketing"><span></span><?php echo $option->label; ?></label>
            <div class="help-block with-errors"></div>
        </div>
<?php
    }   // foreach
    if ($this->config->item('settings_parsed')->agree_terms) :
        $showRequiredMsg = true;
        if (isset($_POST['mac'])) {
            // we've come back around to the form
            // if the $_POST item is set, then the marketing option is ticked, otherwise it was unticked
            $checked = isset($_POST['agree_terms']);
        } else {
            $checked = false;
        }
?>
            <div class="form-group relative">
                <input type="checkbox" id="agree_terms" name="agree_terms" <?php if ($checked) echo ' checked';?> required data-error="You must agree the terms &amp; conditions" />
                <label for="agree_terms" class="marketing"><span></span>By using this service, you agree to the <a class="terms-modal" href="#">terms and conditions</a> of use.</label>
                <div class="help-block with-errors"></div>
            </div>
<?php
    endif;
?>
            </div>
            <div class="col-xs-12 register">
                <button class="btn register-btn" id="sign-up" type="submit">Register</button>
            </div>
        </div>

        </div>
<input type="hidden" name="mac" value="<?php echo $_SESSION['mac']; ?>" />
</form>
