<?php
    echo form_open('subscriberLogin');
?>
<div class="panel panel-default">
    <div class="panel-header text-center">
        <h1>Staff Access</h1>
        <p>This service is available to NHS staff. Please enter your details below for WiFi access. See our FAQs for further information.</p>
    </div>
    <div class="panel-body">
    <div class="standard-inputs">
<?php
    // get the form details from parsed json settings
    $form_items = $this->config->item('settings_parsed')->subscriber_form;
    $showRequiredMsg = false;
    foreach ($form_items as $form_item) {
        switch ($form_item->name) {
            case 'username':
                $columnWidth = 'col-xs-12';
                $style = '';
                $mobile_padding = '';
                $placeholder = 'Email address';
                break;
            case 'password':
                $columnWidth = 'col-xs-12';
                $style = '';
                $mobile_padding = '';
                $placeholder = $form_item->placeholder;
                break;
        }
?>
            <div class="form-group has-feedback<?php
            if (form_error($form_item->name)) echo ' has-error';
            echo ('select' == $form_item->type)?' sel_wrap':' fl_wrap';
            echo ' '.$columnWidth.' '.$mobile_padding; ?>" id="<?php echo $form_item->name; ?>-wrapper" style="z-index:99;<?php echo $style; ?>">
                <?php
                    if ('select' != $form_item->type) :
                ?>
                <input class="form-control fl_input"
                 type="<?php echo $form_item->type ?>"
                 id="<?php echo $form_item->name; ?>"
                 name="<?php echo $form_item->name; ?>"
                 value="<?php echo set_value($form_item->name); ?>"
                 placeholder="<?php echo $placeholder; ?>"
                 onchange="<?php echo $form_item->on_change; ?>"
                 data-error="<?php echo $form_item->client_validation_error; ?>"
                 <?php
                    if ($form_item->client_validation_regex) {
                        echo 'pattern="' . $form_item->client_validation_regex . '"';
                    }
                    if ($form_item->required) {
                        echo ' required';
                        $showRequiredMsg = true;
                    }
                 ?> />
                <?php
                    else :
                        echo '<select id="' . $form_item->name . '"';
                        echo ' name="' . $form_item->name . '"';
                        echo ' onchange="' . $form_item->on_change . '"';
                        if ($form_item->required) {
                            echo ' required';
                            $showRequiredMsg = true;
                            echo ' data-error="' . $form_item->client_validation_error . '"';
                        }
                        echo ' class="form-control">';
                        foreach ($form_item->options as $option) {
                            echo '<option value="' . $option->value . '"' . (($option->value == $form_item->default)?'selected':'') . (isset($option->disabled)?'disabled':'') .'>' . $option->label . '</option>';
                        }
                        echo '</select>';
                ?>
                <?php
                    endif;
                ?>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
<?php
    }   // foreach
?>
<div class="col-xs-12" style="margin-bottom: 15px;">
<div class="top-gap">
    <?php
        if ($this->config->item('settings_parsed')->agree_terms) :
            $showRequiredMsg = true;
            if (isset($_POST['mac'])) {
                // we've come back around to the form
                // if the $_POST item is set, then the marketing option is ticked, otherwise it was unticked
                $checked = isset($_POST['user_def10']);
            } else {
                $checked = false;
            }
        ?>
        <div class="form-group relative">
            <p class="agreed-text">By using this service you agree to our <a href="" data-toggle="modal" data-target="#termsModal" class="misc-link inline-misc">terms and conditions</a>.</p>
        </div>
        <?php
        endif;
    ?>
</div>
<button class="btn btn-register" id="sign-up" type="submit">LOGIN</button>
</div>
<div class="row">
  <div class="bottom text-center">
    <div>
      <img class="img-responsive" src="/assets/images/egtonlogo.png" alt="Logo" style="margin: 16px auto 0;width: 125px;" />
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<input type="hidden" name="mac" value="<?php echo $_SESSION['mac']; ?>" />
</form>
