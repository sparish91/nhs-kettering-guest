<div class="row">
  <div class="nhs-header col-xs-12 col-md-6">
    <h1 id="nhs-header"> NHS Staff WiFi Access</h1>
  </div>

        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-5 col-md-offset-0 right-content">
          <div id="breadcrumbs-nav">
              <div class="wizard">
                  <div class="wizard-inner">
                      <div class="connecting-line"></div>
                      <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active">
                              <a><span class="square-step">1</span></a>
                              <label class="breadcrumb-label">Register</label>
                          </li>
                          <li role="presentation" class="active">
                              <a><span class="square-step">2</span></a>
                              <label class="breadcrumb-label">Validate Email</label>
                          </li>
                          <li role="presentation">
                              <a><span class="square-step">3</span></a>
                              <label class="breadcrumb-label">Validated</label>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>

                    <div class="panel panel-default">
                        <div class="panel-header panel-h1">
                            <div class="row">
                              <div class="col-xs-12">
                                  <h1>Validate email</h1>
                                </div>
                              </div>
                            </div>
                            <div class="panel-header">
                              <div class="row">
                                <div class="col-xs-12">
                                    <p>
                                        Please check your email and click on the validation link to be granted <?php echo $validated_days; ?> days of free internet access.
                                    </p>
                                    <p>
                                        You have been granted <span id="orange-txt"><?php echo $validation_minutes; ?> minutes</span> of internet access to complete the email validation. Click the button below to begin your session.
                                    </p>
                                </div>
                            </div>
                            <a class="btn connect" href="<?php echo $landing_page;?>">Continue</a>
                        </div>
                    </div>
</div>
</div>

<div class="col-xs-12 col-md-7 footer-info-main">
    <a href="http://www.wifispark.com/"><img class="footer-image" src="/assets/images/wifilogo.png"></a>
    <p>WiFi Helpdesk: 0344 848 9555</p>
    <?php
    if ($this->config->item('settings_parsed')->friendly_wifi) :
    ?>
    <a href="http://www.friendlywifi.com/"><img class="center-block friendly" src="/assets/images/friendly_wifi.png" alt="Friendly WiFi" /></a>
    <?php
    endif;
    ?>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
