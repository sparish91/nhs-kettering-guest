<div class="registration center-block">
<?php include("include/formerrors.php"); /////////////////////////// form error messages ?>
<?php
    // show flashdata messages if necessary
    if($this->session->flashdata('msg')):
?>
<h1 id="nhs-header"> NHS Staff WiFi Access</h1>

<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
</div>
<?php
    endif;
?>
<div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-5 col-md-offset-0 right-content">
            <div id="breadcrumbs-nav">
                <div class="wizard">
                    <div class="wizard-inner">
                        <div class="connecting-line"></div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a><span class="square-step">1</span></a>

                            </li>
                            <li role="presentation">
                                <a><span class="square-step">2</span></a>

                            </li>
                            <li role="presentation">
                                <a><span class="square-step">3</span></a>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>

<?php include("include/login_form.php"); /////////////////////////// form itself ?>
</div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
<script src="/assets/lib/js/jquery.FlowupLabels.js"></script>
<script src="/assets/lib/js/validator.min.js"></script>
<script type="text/javascript">
    $(window).load(function () {
        $('form').validator().off('input.bs.validator change.bs.validator');
        $('form').validator().on('submit', showSpinner);
        $('.FlowupLabels').FlowupLabels();
        // since javascript is executing we can remove the no-js class from the body
        $('body').removeClass('no-js');
        // this makes sure that the labels get out of the way when form is filled by autocomplete
        $('input').on('change', function() {
            $('.fl_input').trigger('blur.flowupLabelsEvt');
        });
    });
    function showSpinner(e) {
        // don't let the form be submitted if there is no postcode when there should be
        if (!e.isDefaultPrevented() && ('GB' == $('#country').val()) && ('' == $('#zip_code').val())) {
            $('#zip_code-wrapper').addClass('has-error');
            $('#zip_code').focus();
            e.preventDefault();
        }
        // if the form is valid then cover the display and show a spinner
        if (!e.isDefaultPrevented()) {
            $('body').append('<div class="spinner-container"><div class="spinner"><img src="/assets/images/spinner.gif" /></div></div>');
            $('body').addClass('noscroll');
            $('#registering').attr('disabled', 'disabled');
        }
    }
</script>
