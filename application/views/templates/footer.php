<?php
if ($this->config->item('settings_parsed')->friendly_wifi) :
?>
<!-- <div class="row">
    <div class="col-xs-12">
        <a href="http://www.friendlywifi.com/"><img class="center-block friendly" src="/assets/images/friendly_wifi.png" alt="Friendly WiFi" /></a>
    </div>
</div> -->
<?php
endif;
?>

</div><!-- container -->

<div class="big-modal" id="instructions">
    <a class="close-button" href="#">
        <img src="/assets/images/close.png" alt="">
    </a>
    <div class="container modal-wrapper">
    </div>
</div>
<script>
    $(document).on('click', '.terms-modal', function(event) {
        event.preventDefault();
        $('.big-modal').fadeIn('slow');
        $('.big-modal .modal-wrapper').load('/assets/terms-and-conditions-English-2015.html');
    });
</script>
<script>
    $(document).on('click', '.help-modal', function(event) {
        event.preventDefault();
        $('.big-modal').fadeIn('slow');
        $('.big-modal .modal-wrapper').load('/assets/help.html');
    });
</script>
<script>
    $(document).on('click', '.instructions-modal', function(event) {
        event.preventDefault();
        $('.big-modal').fadeIn('slow');
        $('.big-modal .modal-wrapper').load('/assets/instructions.html');
    });
</script>
<script>
    $(document).on('click', '.close-button', function(event) {
        event.preventDefault();
        $('.big-modal').fadeOut('slow');
    });
</script>
<script type="text/javascript">
$(document).ready(function () {
    $('.menu-burger').click(function () {
        $('.menu').addClass('menu-open');
    });
    $('.menu .close').click(function () {
        $('.menu').removeClass('menu-open');
    });
    $.ajax({
        url: "/pageview/<?php echo $page; ?>"
    });
});
</script>
<script type="text/javascript">
    $(function() {
        // this will get the full URL at the address bar
        var url = window.location.href;

        // passes on every "a" tag
        $(".menu li a").each(function() {
            // checks if its the same on the address bar
            if (url == (this.href)) {
                $(this).closest("li").addClass("active");
            }
        });
    });
</script>
<script src="/assets/lib/bootstrap/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/lib/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
