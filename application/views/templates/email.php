<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>WiFi registration validation required.</title>
    </head>
    <body>
        <p style="width: 200px; padding-bottom: 20px;">
            <a href="http://www.google.co.uk/"><img style="width: 100%;" src="cid:<?php echo $logo_cid; ?>" alt="NHS Logo" /></a>
        </p>
        <h3>Dear WiFi User</h3>
        <p>
            Click the link below to validate the device you registered on the network.
        </p>
        <p>
            <a href="<?php echo $link ?>">Validate my email!</a>
        </p>
        <p>
            Thank you for connecting.
        </p>
        <p>
            <strong>NHS Trust</strong>
        </p>
        <hr />
        <?php
            if ($friendly_wifi) :
        ?>
        <p>
            <a href="http://www.friendlywifi.com/"><img src="cid:<?php echo $friendly_cid; ?>" alt="Friendly Wifi" /></a>
        </p>
        <?php
            endif;
        ?>
    </body>
</html>
