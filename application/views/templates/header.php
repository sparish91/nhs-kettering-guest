<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="shortcut icon" href="/favicon.ico" />
<title><?php echo $title; ?></title>
<link href="/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link href="/assets/lib/css/ie10-viewport-bug-workaround.css" rel="stylesheet" />
<link href="/assets/lib/css/jquery.FlowupLabels.css" rel="stylesheet" />
<link href="/assets/css/style.css" rel="stylesheet" />
<!-- Font awesome icons -->
<link rel="stylesheet" href="/assets/fonts/font-awesome/css/font-awesome.min.css" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="/assets/lib/js/html5shiv.min.js"></script>
  <script src="/assets/lib/js/respond.min.js"></script>
<![endif]-->
<link rel="apple-touch-icon" sizes="57x57" href="/assets/icons/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="60x60" href="/assets/icons/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="72x72" href="/assets/icons/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="/assets/icons/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="/assets/icons/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="/assets/icons/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="/assets/icons/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="/assets/icons/apple-touch-icon-152x152.png" />
<link rel="apple-touch-icon" sizes="180x180" href="/assets/icons/apple-touch-icon-180x180.png" />
<link rel="apple-touch-icon-precomposed" href="/assets/icons/apple-touch-icon-precomposed.png" />
<link rel="icon" type="image/png" href="/assets/icons/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="/assets/icons/favicon-194x194.png" sizes="194x194" />
<link rel="icon" type="image/png" href="/assets/icons/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="/assets/icons/android-chrome-192x192.png" sizes="192x192" />
<link rel="icon" type="image/png" href="/assets/icons/favicon-16x16.png" sizes="16x16" />
<link rel="manifest" href="/assets/icons/manifest.json" />
<link rel="mask-icon" href="/assets/icons/safari-pinned-tab.svg" color="#ff0000" />
<link rel="shortcut icon" href="/assets/icons/favicon.ico" />
<meta name="msapplication-TileColor" content="#ffffff" />
<meta name="msapplication-TileImage" content="/assets/icons/mstile-144x144.png" />
<meta name="msapplication-config" content="/assets/icons/browserconfig.xml" />
<meta name="theme-color" content="#ffffff" />
</head>
<body class="no-js">
<div id="background"></div>
<div class="logo">
    <img class="gm-logo" src="/assets/images/logo.png">
</div>
<div class="container">
<div class="row masthead">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
            <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li><a class="menu-links instructions-modal" href="#">Instructions</a></li>
            <li><a class="mobile-none">|</a></li>
            <li><a class="menu-links help-modal" href="#">Help & FAQ's</a></li>
          </ul>
        </div>
      </div>
    </nav>
</div>
