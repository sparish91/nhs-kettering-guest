<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require 'vendor/autoload.php';
//https://github.com/browscap/browscap-php/tree/2.x
use phpbrowscap\Browscap;

//Location of browscap.ini file
CONST BROWSCAP_CACHE = 'browscap-cache';

/**
 * SPARK API Codeigniter library v0.3
 *
 * @package    Spark
 * @copyright  2003-2016 WifiSpark Limited. All rights reserved.
 * @license    http://www.wifispark.com/license/spark-1.00.txt  SPARK License 1.00
 * @version    GIT: $Id$
 */

class SparkAPI
{
    private $ci;
    private $settings;
    private $browscap;

    /**
     * SparkAPI($params)
     *
     * @param Array $params - ['config'] set to location of .ini file
     */
    public function __construct($spark_api_config)
    {
        $this->ci =& get_instance();

        $this->settings = $spark_api_config;

        $this->browscap = new Browscap(BROWSCAP_CACHE);
        //browscap.ini is 9 MB, no need to update all the time.
        //From time to time we can delete browscap.ini & cache.php so they can be renewed automatically
        $this->browscap->doAutoUpdate = false;
    }

    /**
     * getInterfaceID($ip)
     * Return the interface ID for the (potentially NATed) IP provided by Flint.
     *
     * @param string $ip - IP address provided by Flint
     *
     * @return integer $id - interface ID for the hotspot
     */
    public function getInterfaceID($ip) {
        $hotspots = (array) $this->ci->config->item('settings_parsed')->hotspot_details;
        return $hotspots[$ip]->interface_id;
    }

    /**
     * getToken()
     * Get oAuth token from SPARK API
     *
     * @return string $token or false if error
     */
    private function getToken()
    {
        if ( isset( $this->settings['spark_api']['token'] ) ) {
            return $this->settings['spark_api']['token'];
        }

        log_message('debug', 'getToken() token expires : ' . (isset($_SESSION['sparkapi_token_expires'])?$_SESSION['sparkapi_token_expires']:'no token set'));
        log_message('debug', 'getToken() time now      : ' . time());
        if (isset($_SESSION['sparkapi_token_expires']) && (time() > $_SESSION['sparkapi_token_expires'])) {
            unset($_SESSION['sparkapi_token']);
            unset($_SESSION['sparkapi_token_expires']);
        }
        if (isset($_SESSION['sparkapi_token'])) {
            return $_SESSION['sparkapi_token'];
        }

        $token_url = $this->settings['spark_api']['endpoint'] . '/token';

        $curl_resource = curl_init($token_url);

        $oauth = array(
            'grant_type' => 'password',
            'scope' => $this->settings['oauth']['scope'],
            'client_id' => $this->settings['oauth']['client_id'],
            'client_secret' => $this->settings['oauth']['client_secret'],
            'username' => $this->settings['oauth']['username'],
            'password' => $this->settings['oauth']['password']
        );

        $json_data = json_encode($oauth);

        curl_setopt($curl_resource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_resource, CURLOPT_POST, true);
        curl_setopt($curl_resource, CURLOPT_POSTFIELDS, $json_data);
        $headers = array('Content-Type: application/json', 'Accept:application/json');
        curl_setopt($curl_resource, CURLOPT_HTTPHEADER, $headers);

        $curl_response = curl_exec($curl_resource);
        $return = false;

        if ($curl_response !== false) {
            $result = json_decode($curl_response, true);
            if (isset($result['access_token'])) {
                $_SESSION['sparkapi_token'] = $result['access_token'];
                $_SESSION['sparkapi_token_expires'] = time() + $result['expires_in'];
                $return = $_SESSION['sparkapi_token'];
                log_message('debug', 'token expires : ' . date('Y-m-d H:m:s', $_SESSION['sparkapi_token_expires']));
            } else {
                log_message('error', 'SparkAPI - getToken() : ' . $curl_response);
            }
        } else {
            log_message('error', 'SparkAPI - getToken() - curl error');
        }

        curl_close($curl_resource);
        return $return;
    }

    /**
     * getSubscriberToken()
     * Get oAuth token for subscriber from SPARK API
     *
     * @return string $token or false if error
     */
    private function getSubscriberToken($hotspot_ip, $username, $password)
    {
        log_message('debug', 'getSubscriberToken() token expires : ' . (isset($_SESSION['subscriber_token_expires'])?$_SESSION['subscriber_token_expires']:'no token set'));
        log_message('debug', 'getSubscriberToken() time now      : ' . time());
        if (isset($_SESSION['subscriber_token_expires']) && (time() > $_SESSION['subscriber_token_expires'])) {
            unset($_SESSION['subscriber_token']);
            unset($_SESSION['subscriber_token_expires']);
        }
        if (isset($_SESSION['subscriber_token'])) {
            return $_SESSION['subscriber_token'];
        }

        $token_url = $this->settings['spark_api']['endpoint'] . '/token';

        $curl_resource = curl_init($token_url);

        $oauth = array(
            'grant_type' => 'subscriber',
            'scope' => $this->settings['oauth']['scope'],
            'hotspot_ip' => $hotspot_ip,
            'client_id' => $this->settings['oauth']['client_id'],
            'client_secret' => $this->settings['oauth']['client_secret'],
            'username' => $username,
            'password' => $password
        );

        $json_data = json_encode($oauth);

        curl_setopt($curl_resource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_resource, CURLOPT_POST, true);
        curl_setopt($curl_resource, CURLOPT_POSTFIELDS, $json_data);
        $headers = array('Content-Type: application/json', 'Accept:application/json');
        curl_setopt($curl_resource, CURLOPT_HTTPHEADER, $headers);

        $curl_response = curl_exec($curl_resource);

        $return = false;

        if ($curl_response !== false) {
            $result = json_decode($curl_response, true);
            if (isset($result['access_token'])) {
                $_SESSION['subscriber_token'] = $result['access_token'];
                $_SESSION['subscriber_token_expires'] = time() + $result['expires_in'];
                $return = $_SESSION['subscriber_token'];
                log_message('debug', 'subscriber token expires : ' . date('Y-m-d H:m:s', $_SESSION['subscriber_token_expires']));
            } else {
                log_message('error', 'SparkAPI - getSubscriberToken() : ' . $curl_response);
            }
        } else {
            log_message('error', 'SparkAPI - getSubscriberToken() - curl error');
        }

        curl_close($curl_resource);
        return $return;
    }

    /**
     * getSubscriber($subscriber_id)
     * Get a subcriber from SPARK
     *
     * @param integer $subscriber_id
     *
     * @return stdClass Object $subscriber - returned subscriber details from SPARK
     */
    public function getSubscriber($subscriber_id)
    {
        $token = $this->getToken();
        $subscriber_url = $this->settings['spark_api']['endpoint'] . '/subscribers/' . $subscriber_id;

        $curl_resource = curl_init($subscriber_url);

        curl_setopt($curl_resource, CURLOPT_RETURNTRANSFER, true);
        $headers = array('Authorization: OAuth2 Bearer ' . $token, 'Content-Type: application/json', 'Accept:application/json');
        curl_setopt($curl_resource, CURLOPT_HTTPHEADER, $headers);

        $curl_response = curl_exec($curl_resource);
        curl_close($curl_resource);

        $subscriber = json_decode($curl_response);

        if (null == $subscriber) log_message('error', 'SparkAPI - getSubscriber() - ' . $curl_response);

        log_message('debug', 'get subscriber : '. $subscriber->id . ' ' . $subscriber->login);

        return $subscriber;
    }

    /**
     * searchSubscriber($search)
     * Search for a subscriber in SPARK
     *
     * @param Array $search - associative array of keys/values to search
     *
     * @return stdClass Object $subscriber - returned subscriber details from SPARK or false if not found
     */
    public function searchSubscriber($search = Array())
    {
        $token = $this->getToken();
        $subscriber_url = $this->settings['spark_api']['endpoint'] . '/subscribers';

        if (!empty($search)) {
            $subscriber_url .= '?';
            foreach ($search as $key => $value) {
                $subscriber_url .= $key . '=' . $value . '&';
            }
            $subscriber_url = substr($subscriber_url, 0, -1);
        }

        $curl_resource = curl_init($subscriber_url);

        curl_setopt($curl_resource, CURLOPT_RETURNTRANSFER, true);
        $headers = array('Authorization: OAuth2 Bearer ' . $token, 'Content-Type: application/json', 'Accept:application/json');
        curl_setopt($curl_resource, CURLOPT_HTTPHEADER, $headers);

        $curl_response = curl_exec($curl_resource);
        curl_close($curl_resource);

        $json_data = json_decode($curl_response);
        if (null == $json_data) {
            log_message('error', 'SparkAPI - searchSubscriber() - ' . $curl_response);
            return false;
        }
        if (isset($json_data->error)) {
            log_message('error', 'SparkAPI - searchSubscriber() - ' . print_r($json_data, true));
            return false;
        }
        if (is_object($json_data) && property_exists($json_data, 'items')) {
            $json_data = $json_data->items;
        }
        // if result is an empty array then subscriber wasn't found
        if (empty($json_data)) return false;

        // grab the subscriber id and then make another curl request to get the actual subscriber data
        $subscriber_id = intval(substr($json_data[0]->uri, 13));

        return $this->getSubscriber($subscriber_id);
    }

    /**
     * createSubscriber($subscriber_data)
     * Insert a new subscriber into SPARK
     *
     * @param Array $subscriber_data - expects at least ['login'], ['password'], ['hotspot_ip']
     *
     * @return integer $subscriber_id or false if failed
     */
    public function createSubscriber($subscriber_data)
    {
        $token = $this->getToken();
        $subscriber_url = $this->settings['spark_api']['endpoint'] . '/subscribers';

        $curl_resource = curl_init($subscriber_url);

        $json_data = json_encode($subscriber_data);

        $headers = array('Authorization: OAuth2 Bearer ' . $token, 'Content-Type: application/json', 'Accept:application/json');
        curl_setopt($curl_resource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_resource, CURLOPT_POST, true);
        curl_setopt($curl_resource, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($curl_resource, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl_resource, CURLOPT_HEADER, true);

        $curl_response = curl_exec($curl_resource);
        curl_close($curl_resource);
        if (substr($curl_response, 0, 10) != 'HTTP/1.1 2') {        // accept all 2xx codes as success
            log_message('error', 'SparkAPI - createSubscriber() - ' . $curl_response);
            return false;
        }

        $search = 'Location: /subscribers/';
        $subscriber_id = intval(substr($curl_response, strpos($curl_response, $search)+strlen($search)));

        log_message('debug', 'create subscriber : ' . substr($curl_response, 0, 12));

        return $subscriber_id;
    }

    /**
     * updateSubscriber($subscriber_id, $update_data)
     * Update an existing subscriber in SPARK
     *
     * @param integer $subscriber_id
     * @param Array $update_data - associative array of keys and values to be updated
     *
     * @return boolean success
     */
    public function updateSubscriber($subscriber_id, $update_data)
    {
        $token = $this->getToken();
        $update_url = $this->settings['spark_api']['endpoint'] . '/subscribers/' . $subscriber_id;

        $curl_resource = curl_init($update_url);

        $json_data = json_encode($update_data);

        $headers = array('Authorization: OAuth2 Bearer ' . $token, 'Content-Type: application/json', 'Accept:application/json');
        curl_setopt($curl_resource, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl_resource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_resource, CURLOPT_POST, true);
        curl_setopt($curl_resource, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($curl_resource, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl_resource, CURLOPT_HEADER, true);

        $curl_response = curl_exec($curl_resource);
        curl_close($curl_resource);

        if (substr($curl_response, 0, 10) == 'HTTP/1.1 2') {        // accept all 2xx codes as success
            return true;
        } else {
            log_message('error', 'SparkAPI - updateSubscriber() - ' . $curl_response);
            return false;
        }
    }

    /**
     * activateSubscriber($subscriber_id, $activation_data)
     * Activate a subsriber and give them access to the Internet
     *
     * @param integer $subscriber_id
     * @param Array $activation_data - array that mirrors JSON required http://apidocs.wifispark.net/resources/subscribers_activation.html
     *
     * @return boolean true for successful activation
     */
    public function activateSubscriber($subscriber_id, $activation_data)
    {
        $token = $this->getToken();
        $activate_url = $this->settings['spark_api']['endpoint'] . '/subscribers/' . $subscriber_id . '/activation';

        $curl_resource = curl_init($activate_url);

        $json_data = json_encode($activation_data);

        $headers = array('Authorization: OAuth2 Bearer ' . $token, 'Content-Type: application/json', 'Accept:application/json');
        curl_setopt($curl_resource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_resource, CURLOPT_POST, true);
        curl_setopt($curl_resource, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($curl_resource, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl_resource, CURLOPT_HEADER, true);

        $curl_response = curl_exec($curl_resource);
        curl_close($curl_resource);

        log_message('debug', 'activate : ' . substr($curl_response, 0, 12));

        if (substr($curl_response, 0, 10) == 'HTTP/1.1 2') {        // accept all 2xx codes as success
            return true;
        } else {
            log_message('error', 'SparkAPI - activateSubscriber() - ' . $curl_response);
            return false;
        }
    }

    /**
     * deleteSubscriber($subscriber_id)
     * Deletes a subscriber from Spark
     *
     * @param $subscriber_id - unique ID of the subscriber to delete
     */
    public function deleteSubscriber($subscriber_id)
    {
        $token = $this->getToken();
        $delete_url = $this->settings['spark_api']['endpoint'] . '/subscribers/' . $subscriber_id;

        $curl_resource = curl_init($delete_url);

        $headers = array('Authorization: OAuth2 Bearer ' . $token, 'Content-Type: application/json', 'Accept:application/json');
        curl_setopt($curl_resource, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl_resource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_resource, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl_resource, CURLOPT_HEADER, true);

        $curl_response = curl_exec($curl_resource);
        curl_close($curl_resource);

        unset($_SESSION['subscriber_token']);
        unset($_SESSION['subscriber_token_expires']);

        if (substr($curl_response, 0, 10) == 'HTTP/1.1 2') {        // accept all 2xx codes as success
            log_message('debug', 'deleted subscriber : ' . $subscriber_id);
            return true;
        } else {
            log_message('error', 'SparkAPI - deleteSubscriber() - ' . $curl_response);
            return false;
        }
    }

    /**
     * startSession($hotspot_ip, $mac, $username, $password, $interface_id = '')
     * Starts a session for a device on a hotspot
     *
     * @param $hotspot_ip - IP of the hotspot
     * @param $mac - MAC address of the device
     * @param $username - subscriber username
     * @param $password - subscriber password
     * @param $interface_id - interface ID number or empty string
     *
     * @return Boolean - successfully started session
     */
    public function startSession($hotspot_ip, $mac, $username, $password, $interface_id = '')
    {
        $token = $this->getSubscriberToken($hotspot_ip, $username, $password);
        $session_url = $this->settings['spark_api']['endpoint'] . '/sessions';

        $curl_resource = curl_init($session_url);

        $post_data = Array(
            "hotspot_ip" => $hotspot_ip,
            "mac_address" => $mac
        );

        if ($interface_id != '') {
            $post_data['hotspot_interface_id'] = $interface_id;
        }

        $json_data = json_encode($post_data);

        $headers = array('Authorization: Bearer ' . $token, 'Content-Type: application/json');
        curl_setopt($curl_resource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_resource, CURLOPT_POST, true);
        curl_setopt($curl_resource, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($curl_resource, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl_resource, CURLOPT_HEADER, true);

        $curl_response = curl_exec($curl_resource);
        curl_close($curl_resource);

        log_message('debug', 'start session : ' . substr($curl_response, 0, 12) . ' : ' . substr($curl_response, strrpos($curl_response, '{"')));

        if (substr($curl_response, 0, 10) == 'HTTP/1.1 2') {        // accept all 2xx codes as success
            return true;
        } else {
            log_message('error', 'SparkAPI - startSession() - ' . $curl_response);
            return false;
        }
    }

    /**
     * Sending analytics data
     * @param array $analyticsData
     * @param string $hotspot_ip
     * @param string $interface_id
     * @param string $token
     * @throws Exception
     */
    public function sendAnalytics($analyticsData, $hotspot_ip, $interface_id = '', $token='') {
        // throw exception when hotspot_ip or device_mac_address are not provided
        // otherwise the splunk datadaemon will crash
        if (empty($hotspot_ip) || empty($analyticsData['device_mac_address'])) {
            throw new Exception('Analytics error: '.print_r($analyticsData, true));
        }
        if (empty($token)) {
            $token = $this->getToken();
        }

        $analytics_resource = $this->settings['spark_api']['endpoint'] . '/analytics';

        //Init curl
        $curlAnalytics = curl_init($analytics_resource);

        $user_agent_info = $this->browscap->getBrowser();
        $analyticsData['user_agent'] = $user_agent_info;
        $analyticsData['hotspot_ip'] = $hotspot_ip;
        if ($interface_id != '') {
            $analyticsData['interface_id'] = $interface_id;
        }
        $analyticsData['type']='Portal';
        $analyticsData['view_valid'] = 'false';
        $analyticsData['doing_ajax'] = 'false';
        $analyticsJson = json_encode($analyticsData);

        //Set the oauth2 auth header in our request
        $headers = array('Authorization: OAuth2 Bearer ' . $token, 'Content-Type: application/json', 'Accept:application/json');
        curl_setopt($curlAnalytics, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlAnalytics, CURLOPT_POSTFIELDS, $analyticsJson);
        curl_setopt($curlAnalytics, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlAnalytics, CURLOPT_HEADER, true);

        //Our response
        $curlAnalytics_response = curl_exec($curlAnalytics);
        //Throwing exceptions
        if ($curlAnalytics_response === false) {
            $error = 'Analytics error: '.curl_error($curlAnalytics).' : '.print_r($analyticsData, true);
            curl_close($curlAnalytics);
            throw new Exception($error);
        }
        $curlInfo = curl_getinfo($curlAnalytics);
        if ($curlInfo['http_code'] != 200) {
            throw new Exception('Analytics error: '.print_r($analyticsData, true).' : '.print_r($curlAnalytics_response, true));
        }

        curl_close($curlAnalytics);
    }
}
